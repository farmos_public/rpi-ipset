echo -e '\n\n ipset process \n'
cat << "EOF" > "ipset"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          rpi-ipset
# Required-Start:   
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       
### END INIT INFO

EOF

echo "WORK_DIR=\"/home/pi/rpi-ipset\"" >> ipset

cat << "EOF" >> "ipset"
cd "$WORK_DIR"

case "$1" in
  start)
    echo "Starting ipsetting server"
    forever start --uid "ipsetting" --minUptime 1000 --spinSleepTime 1000 -a "${WORK_DIR}/app.js"
    ;;
  stop)
    echo "Stopping ipsetting server"
    forever stop ipsetting
    ;;
  *)
    echo "Usage: /etc/init.d/ipset {start|stop}"
    exit 1
    ;;
esac
exit 0
EOF

sudo chmod +x ipset
sudo mv ipset /etc/init.d/ipset


sudo update-rc.d ipset defaults
sudo /etc/init.d/ipset start

