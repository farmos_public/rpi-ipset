'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
var fs = require('fs');

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  ipset: ipset 
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function ipset(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  console.log(req.swagger.params)
  var method = req.swagger.params.body.value.method || 'dhcp';
  var ip = req.swagger.params.body.value.ipaddress || ''; 
  var gw = req.swagger.params.body.value.gateway || '';
  var nm = req.swagger.params.body.value.netmask || '';

  var prestr = "hostname\nclientid\npersistent\noption rapid_commit\noption domain_name_servers, domain_name, domain_search, host_name\n";
  var prestr = prestr + "option classless_static_routes\noption interface_mtu\nrequire dhcp_server_identifier\nslaac private\n";
  var prestr = prestr + "interface wlan0\nstatic ip_address=10.2.0.1/24\nnohook wpa_supplicant\n";

  var maskNodes = nm.match(/(\d+)/g);
  var cidr = 0;
  for(var i in maskNodes) {
    cidr += (((maskNodes[i] >>> 0).toString(2)).match(/1/g) || []).length;
  }

  ip = ip + "/" + cidr

  var dhcpstr = "";
  if (method == 'static')
    dhcpstr = util.format("interface eth0\nstatic ip_address=%s\nstatic routers=%s\nstatic domain_name_servers=8.8.8.8 168.126.63.1\n", ip, gw);

  console.log(prestr + dhcpstr);
  fs.writeFileSync('/etc/dhcpcd.conf', prestr + dhcpstr);
  console.log("success");
  res.json('{"message":"success"}');
  console.log("reboot");
  require('reboot').reboot();
}
